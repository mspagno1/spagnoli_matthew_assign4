package genericDeser.util;

import genericDeser.fileOperations.Logger;


public class Second{
	private byte byteVal;
	private short shortVal;
	private int intVal;
	private long longVal;
	private float floatVal;
	private double doubleVal;
	private boolean boolVal;
	private char charVal;
	private String strVal;

	private byte byteVal2;
	private short shortVal2;
	private int intVal2;
	private long longVal2;
	private float floatVal2;
	private double doubleVal2;
	private boolean boolVal2;
	private char charVal2;
	private String strVal2;

	public Second(){
		//Logger constructor here thats about it
		Logger.writeMessage("Second constructor called ", Logger.DebugLevel.C_TOR);
		strVal = "";
		strVal2 ="";
	}

	@Override
	public boolean equals(Object obj){
		boolean sameObj = true;
		final Second otherObj = (Second) obj;
		
		/*
		System.out.println("=-=--=-=Equals overide=-=--==-");
		String str1 = toString();
		String str2 = otherObj.toString();
		
		System.out.println("Str 1 " + "\n" + str1);

		System.out.println("Str 2 " + "\n" + str2);		

		*/
		if(byteVal != otherObj.getByteValue()){
			sameObj = false;
		}
		if(shortVal != otherObj.getShortValue()){
			sameObj = false;
		}
		if(intVal != otherObj.getIntValue()){
			sameObj = false;
		}
		if(longVal != otherObj.getLongValue()){
			sameObj = false;
		}
		if(floatVal - otherObj.getFloatValue() > 1.0){
			sameObj = false;
		}
		if(doubleVal - otherObj.getDoubleValue() > 1.0){
			sameObj = false;
		}
		if(boolVal != otherObj.getBooleanValue()){
			sameObj = false;
		}
		if(charVal != otherObj.getCharValue()){
			sameObj = false;
		}
		if(!strVal.equals(otherObj.getStringValue())){
			sameObj = false;
		}

		if(byteVal2 != otherObj.getByteValue2()){
			sameObj = false;
		}
		if(shortVal2 != otherObj.getShortValue2()){
			sameObj = false;
		}
		if(intVal2 != otherObj.getIntValue2()){
			sameObj = false;
		}
		if(longVal2 != otherObj.getLongValue2()){
			sameObj = false;
		}
		if(floatVal2 - otherObj.getFloatValue2() > 1.0){
			sameObj = false;
		}
		if(doubleVal2 - otherObj.getDoubleValue2() > 1.0){
			sameObj = false;
		}
		if(boolVal2 != otherObj.getBooleanValue2()){
			sameObj = false;
		}
		if(charVal2 != otherObj.getCharValue2()){
			sameObj = false;
		}
		if(!strVal2.equals(otherObj.getStringValue2())){
			sameObj = false;
		}
		return sameObj;
	}
		
	@Override
	public int hashCode(){
		int result = 19;
		result = 40 * result; 
		result = 5 * byteVal + byteVal2; 
		result = 19 + (int)doubleVal + (int)longVal; 
		result = result * 15 + (int)doubleVal2 + (int)longVal2;
		return result;
	}

	public void setByteValue(byte bVal){
		byteVal = bVal;
	}

	public byte getByteValue(){
		return byteVal;
	}

	public void setShortValue(short shVal){
		shortVal = shVal;
	}

	public short getShortValue(){
		return shortVal;
	}

	public void setIntValue(int inVal){
		intVal = inVal;
	}

	public int getIntValue(){
		return intVal;
	}
	
	public void setLongValue(long lVal){
		longVal = lVal;
	}

	public long getLongValue(){
		return longVal;
	}

	public void setFloatValue(float fVal){
		floatVal = fVal;
	}

	public float getFloatValue(){
		return floatVal;
	}
		
	public void setDoubleValue(double dVal){
		doubleVal = dVal;
	}

	public double getDoubleValue(){
		return doubleVal;
	}

	public void setBooleanValue(boolean bVal){
		boolVal = bVal;
	}

	public boolean getBooleanValue(){
		return boolVal;
	}

	public void setCharValue(char cVal){
		charVal = cVal;
	}

	public char getCharValue(){
		return charVal;
	}

	public void setStringValue(String sVal){
		strVal = sVal;
	}

	public String getStringValue(){
		return strVal;
	}

	
	public void setByteValue2(byte bVal){
		byteVal2 = bVal;
	}

	public byte getByteValue2(){
		return byteVal2;
	}

	public void setShortValue2(short shVal){
		shortVal2 = shVal;
	}

	public short getShortValue2(){
		return shortVal2;
	}

	public void setIntValue2(int inVal){
		intVal2 = inVal;
	}

	public int getIntValue2(){
		return intVal2;
	}
	
	public void setLongValue2(long lVal){
		longVal2 = lVal;
	}

	public long getLongValue2(){
		return longVal2;
	}

	public void setFloatValue2(float fVal){
		floatVal2 = fVal;
	}

	public float getFloatValue2(){
		return floatVal2;
	}
		
	public void setDoubleValue2(double dVal){
		doubleVal2 = dVal;
	}

	public double getDoubleValue2(){
		return doubleVal2;
	}

	public void setBooleanValue2(boolean bVal){
		boolVal2 = bVal;
	}

	public boolean getBooleanValue2(){
		return boolVal2;
	}

	public void setCharValue2(char cVal){
		charVal2 = cVal;
	}

	public char getCharValue2(){
		return charVal2;
	}

	public void setStringValue2(String sVal){
		strVal2 = sVal;
	}

	public String getStringValue2(){
		return strVal2;
	}

	public String toString(){
		String data = "Byte " + byteVal + "\n" + "Byte2 " + byteVal2 + "\n" + "ShortVal " + shortVal + "\n"+ "ShortVal2 " + shortVal2 + "\n" +"Int " + intVal + "\n" + "Int2 " + intVal2 + "\n" + "LongVal " + longVal + "\n" + "Long2Val " + longVal2 + "\n" + "FloatVal " + floatVal + "\n"+  "FloatVal2 " + floatVal2 + "\n" + "Double " + doubleVal + "\n" + "Double2 " + doubleVal2 + "\n" + "Boolean " + boolVal + "\n" + "Boolean2 " + boolVal2 + "\n" + "Char " + charVal + "\n" + "Char2 " + charVal2 + "\n" +"String " + strVal + "\n" + "String2 " + strVal2;
		return data;
	}

}
