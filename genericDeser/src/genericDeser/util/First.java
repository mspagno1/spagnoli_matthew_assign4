package genericDeser.util;

import genericDeser.fileOperations.Logger;


public class First{
	private byte byteVal;
	private short shortVal;
	private int intVal;
	private long longVal;
	private float floatVal;
	private double doubleVal;
	private boolean boolVal;
	private char charVal;
	private String strVal;

	public First(){
		Logger.writeMessage("First constructor called ", Logger.DebugLevel.C_TOR);
		strVal = "";
	}
	@Override
	public boolean equals(Object obj){
		boolean sameObj = true;
		
		/*
		System.out.println("=-=--=-=Equals overide=-=--==-");
		String str1 = toString();
		String str2 = obj.toString();
		
		System.out.println("Str 1 " + "\n" + str1);

		System.out.println("Str 2 " + "\n" + str2);
		*/

		final First otherObj = (First) obj;		

		if(byteVal != otherObj.getByteValue()){
			sameObj = false;
		}
		if(shortVal != otherObj.getShortValue()){
			sameObj = false;
		}
		if(intVal != otherObj.getIntValue()){
			sameObj = false;
		}
		if(longVal != otherObj.getLongValue()){
			sameObj = false;
		}
		if(floatVal - otherObj.getFloatValue() > 1.0){
			sameObj = false;
		}
		if(doubleVal - otherObj.getDoubleValue() > 1.0){
			sameObj = false;
		}
		if(boolVal != otherObj.getBooleanValue()){
			sameObj = false;
		}
		if(charVal != otherObj.getCharValue()){
			sameObj = false;
		}
		if(!strVal.equals(otherObj.getStringValue())){
			sameObj = false;
		}
		return sameObj;
	}
	@Override
	public int hashCode(){
		int result = 19;
		result = 40 * result; 
		result = 5 * byteVal; 
		result = 6 + shortVal + intVal + (int)doubleVal; 
		result = result * 19 + (int)floatVal + (int)longVal;
		return result;
	}
	
	//Getters and setters for the function

	public void setByteValue(byte bVal){
		byteVal = bVal;
	}

	public byte getByteValue(){
		return byteVal;
	}

	public void setShortValue(short shVal){
		shortVal = shVal;
	}

	public short getShortValue(){
		return shortVal;
	}

	public void setIntValue(int inVal){
		intVal = inVal;
	}

	public int getIntValue(){
		return intVal;
	}
	
	public void setLongValue(long lVal){
		longVal = lVal;
	}

	public long getLongValue(){
		return longVal;
	}

	public void setFloatValue(float fVal){
		floatVal = fVal;
	}

	public float getFloatValue(){
		return floatVal;
	}
		
	public void setDoubleValue(double dVal){
		doubleVal = dVal;
	}

	public double getDoubleValue(){
		return doubleVal;
	}

	public void setBooleanValue(boolean bVal){
		boolVal = bVal;
	}

	public boolean getBooleanValue(){
		return boolVal;
	}

	public void setCharValue(char cVal){
		charVal = cVal;
	}

	public char getCharValue(){
		return charVal;
	}

	public void setStringValue(String sVal){
		strVal = sVal;
	}

	public String getStringValue(){
		return strVal;
	}
	
	public String toString(){
		String data = "Byte " + byteVal + "\n" + "ShortVal " + shortVal + "\n" +"Int " + intVal + "\n" +  "LongVal " + longVal + "\n" + "FloatVal " + floatVal + "\n" + "Double " + doubleVal + "\n" + "Boolean " + boolVal + "\n" + "Char " + charVal + "\n" + "String " + strVal;
		return data;
	}

}
