

#Compilation
From the folder: spagnoli_matthew_assign4/genericDeser/src

ant clean
ant all


TO RUN:
From the folder: spagnoli_matthew_assign4/genericDeser/src

ant run -Darg0=<input-file-name> -Darg1=<output-file-name> -Darg2=<debug_value>

Example:
ant run -Darg0=input.txt -Darg1=out.txt -Darg2=0

The files are read from/to here:
	spagnoli_matthew_assign4/genericDeser
	
	
DEBUG_VALUE=4 [Print how many duplicates for each counter location]
DEBUG_VALUE=3 [Print toString of objects data before we add it to data structure]
DEBUG_VALUE=2 [Print every input line]
DEBUG_VALUE=1 [Print constructor call]
DEBUG_VALUE=0 [No output should be printed from the application, except normal output of the number of objects and the number of duplicates]


CHOICE OF DATA STRUCTURE:
Data structure used is ... because the frequent operations are ... and the time complexity for avg case is ...

I used an arraylist to store the objects First, and Second. I also had two other seperate arraylists for the amount 
of duplicates assiocated those two objects. Hashmaps would have been a better data structure but I was confused about how to avoid the perfect storm of data sharing a hash value.

Arraylist time complexity to compare all elements is n^n  

Matthew Spagnoli
used 1 late day
